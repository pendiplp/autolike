<!DOCTYPE html>
<html lang="en">
  <head>
    {% include "layouts/header.volt" %}
  </head>
  <body>
    {% include "layouts/navbar.volt" %}
    
    {{ content() }}

    {% include "layouts/footer.volt" %}
  </body>
</html>