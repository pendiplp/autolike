    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Casper Liker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav pull-right">
            <li class="active"><a href="{{ url("") }}">Home</a></li>
            <li><a href="{{ url("terms") }}">Terms</a></li>
            <li><a href="{{ url("privacy") }}">Privacy</a></li>
            <li><a href="{{ url("contact") }}">Contact</a></li>
            <li><a href="{{ url("about") }}">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>