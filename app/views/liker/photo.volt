<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">SELECT ALBUM</h3>
		</div>
	</div>
</div>
{% for album in albums %}
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<img src="http://graph.facebook.com/{{ album['from']['id'] }}/picture?type=large&redirect=true&width=120&height=120" alt="fbid" class="img-circle pull-left">
			<h3>{{ album['from']['name'] }}</h3>
			<div>
				{{ album['name'] }}
			</div><br/>
			<a href="{{ url('liker/photo/album/id/') }}{{ album['id'] }}" class="btn btn-primary">CHOSE ALBUM</a>
		</div>
	</div>
</div>
{% endfor %}