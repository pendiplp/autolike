<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">photo LIKER</h3>
		</div>
	</div>
</div>
{% for photo in photos %}
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<img src="http://graph.facebook.com/{{ photo['from']['id'] }}/picture?type=large&redirect=true&width=120&height=120" alt="fbid" class="img-circle pull-left">
			<h3>{{ photo['from']['name'] }}</h3>
			<div>
				{{ photo['name'] }}
			</div><br/>
			<div><img src="{{ photo['picture'] }}" class="img-thumbnail" alt="mypic"></div>
            {{ form('liker/process') }}
	            <select class="col-md-12" name="limit">
	            	<option value="250">Set Your Limit</option>
	            	<option value="50">50</option>
	            	<option value="100">100</option>
	            	<option value="150">150</option>
	            	<option value="200">200</option>
	            	<option value="250">250</option>
	            	<option value="300">300</option>
	            </select>
	            <input type="hidden" name="fbid" value="{{ photo['id'] }}" />
				{{ submit_button('Submit', 'class' : 'btn btn-primary') }}
			{{ end_form() }}
		</div>
	</div>
</div>
{% endfor %}
