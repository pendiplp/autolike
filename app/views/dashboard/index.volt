    <!-- Content -->
    <div class="container text-center">
    	<div class="jumbotron">
    		<img src="{{ url("img/casper.png") }}" class="img-rounded">
    		<h1>Welcome to Dashboard</h1>
    		<p class="lead">Select One Of Service As You Want</p>
    	</div>
    </div>

    <div class="container text-center">
    	<div class="panel panel-info">
    		<div class="panel-heading">
    			<h3 class="panel-title">Dashboard</h3>
    		</div>
    		<div class="panel-body">
    			<div class="input-btn">
                    {{ link_to("liker/status", "STATUS LIKER", "class" : "btn btn-primary") }}
                    {{ link_to("liker/photo", "PHOTO LIKER", "class" : "btn btn-primary") }}
                    {{ link_to("liker/video", "VIDEO LIKER", "class" : "btn btn-primary") }}
                </div>
                <br/>
                <div class="input-btn">
                    {{ link_to("liker/note", "NOTE LIKER", "class" : "btn btn-primary") }}
                    {{ link_to("liker/page
                    ", "FANSPAGE POST LIKER", "class" : "btn btn-primary") }}
                    {{ link_to("liker/custom", "CUSTOM LIKER", "class" : "btn btn-primary") }}
    			</div>
    			<br/>
    			<div class="input-btn">
                    {{ link_to("session/end", "LOGOUT", "class" : "btn btn-warning") }}
    				<!-- <button type="Button" class="btn btn-warning">LOGOUT</button> -->
    			</div>


    		</div>
    	</div>
    </div>