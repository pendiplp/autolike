<?php

class Helpers
{

	public function curl($url, $post=null, $header=false){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if ($post != null) {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		if ($header != false) curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$exec = curl_exec($ch);
		if (curl_errno($ch)) {
			return json_encode(array("error" => array("message" => curl_error($ch) )));
		}
		curl_close($ch);
		return $exec;
	}
}