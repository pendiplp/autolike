<?php

class Feeds extends \Phalcon\Mvc\Model
{
	private $access_token;

	private $helper;

	public function initialize()
	{
		$this->helper 		= $this->getDI()->getHelper();
		$auth				= $this->getDI()->getSession()->get('auth');
		$this->access_token	= $auth['access_token'];
	}

	public function getStatus($uid = "me")
	{
		$feeds = json_decode($this->helper->curl('https://graph.facebook.com/' . $uid . '/feed?fields=id,message,picture,link,name,description,type,icon,created_time,from,object_id,privacy&limit=1000&access_token=' . $this->access_token), true);
		
		$status = array();
		foreach ($feeds['data'] as $feed) {
			if ($feed['privacy']['value'] == "EVERYONE") {
				$message = @$feed['message'] ? $feed['message'] : @$feed['description'];
				$data = array(
					'fbid'		=> $feed['id'],
					'from'		=> $feed['from'],
					'message'	=> substr($message, 0, 500)
				);
				array_push($status, $data);
			}

			if (count($status) == 10) break;
		}		
		return $status;
	}

	public function getPhotoAlbum()
	{
		$feeds = json_decode($this->helper->curl('https://graph.facebook.com/me/albums?access_token=' . $this->access_token), true);
		
		$album = array();
		foreach ($feeds['data'] as $feed) {
			if ($feed['privacy'] == "everyone") {
				$data = array(
					'id'		=> $feed['id'],
					'from'		=> $feed['from'],
					'name'		=> substr($feed['name'], 0, 500)
				);
				array_push($album, $data);
			}
		}		
		return $album;
	}

	public function getPhotos($album_id)
	{
		$feeds = json_decode($this->helper->curl('https://graph.facebook.com/' . $album_id . '/photos?access_token=' . $this->access_token), true);

		$photos = array();
		foreach ($feeds['data'] as $feed) {
			$data = array(
				'id'		=> $feed['from']['id'] . "_" . $feed['id'],
				'from'		=> $feed['from'],
				'picture'	=> $feed['picture'],
				'name'		=> substr($feed['name'], 0, 500)
			);
			array_push($photos, $data);
		}		
		return $photos;
	}

	public function getPageID()
	{
		$feeds = json_decode($this->helper->curl('https://graph.facebook.com/me/accounts?fields=id,name,category&access_token=' . $this->access_token), true);

		return $feeds['data'];
	}

	public function getNotes()
	{
		$feeds = json_decode($this->helper->curl('https://graph.facebook.com/me/notes?fields=id,from,subject&access_token=' . $this->access_token), true);

		$notes = array();
		foreach ($feeds['data'] as $feed) {
			$data = array(
				'id'		=> $feed['from']['id'] . "_" . $feed['id'],
				'from'		=> $feed['from'],
				'subject'	=> $feed['subject']
			);
			array_push($notes, $data);
		}		
		return $notes;
	}

	public function getVideos()
	{
		$feeds = json_decode($this->helper->curl('https://graph.facebook.com/me/videos/uploaded?fields=id,from,picture,source&access_token=' . $this->access_token), true);

		$videos = array();
		foreach ($feeds['data'] as $feed) {
			$data = array(
				'id'		=> $feed['id'],
				'from'		=> $feed['from'],
				'src' 		=> $feed['picture']
				// 'src'		=> @$feed['source'] ? $feed['source'] : @$feed['picture']
			);
			array_push($videos, $data);
		}
		return $videos;
	}
}
