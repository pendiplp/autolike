<?php

use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Role;

$acl = new AclList();

// Default action is Deny access
$acl->setDefaultAction(Phalcon\Acl::DENY);

// Adding Roles to the ACL
$roleAdmins = new Role("Administrators", "Super-User Role");
$roleGuests = new Role("Guests");

// Add "Guests" role to Acl
$acl->addRole($roleGuests);
$acl->addRole("Designer");

echo "<pre>".print_r($acl, 1)."</pre>";die();