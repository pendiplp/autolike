<?php

use Phalcon\Mvc\Router;

$router = new Router();

$router->add("/terms", "page::terms");
$router->add("/privacy", "page::privacy");
$router->add("/contact", "page::contact");
$router->add("/about", "page::about");

$router->add("/liker/photo/album/id/{album_id:[0-9]+}", "liker::photoAlbum");
$router->add("/liker/page/id/{page_id:[0-9]+}", "liker::pageStatus");

return $router;