<?php

class SessionController extends ControllerBase
{

	public function initialize()
	{
		$this->view->disable();
	}

	public function indexAction()
	{
		$this->response->redirect();
	}

	private function _parseToken($token)
	{
		if (preg_match("/access_token=(.*)&expires_in/", $token, $matches)) {
			$validToken = $matches[1];
		} else {
			$validToken = $token;
		}

		return $validToken;
	}

	private function _verifyToken($access_token)
	{
		// Verify step-1
		$app_id = json_decode($this->helper->curl('https://graph.facebook.com/app?fields=id&access_token='.$access_token), true);
		if (@$app_id['error'] != false) return false;
		if (!preg_match('/200758583311692|41158896424/', $app_id['id'])) return false;

		// Verify step-2
		$app_permission = json_decode($this->helper->curl('https://graph.facebook.com/me/permissions?fields=publish_actions,offline_access&access_token='.$access_token), true);
		if (@$app_permission['error'] != false) return false;
		if ($app_permission['data'][0]['publish_actions'] != 1 OR $app_permission['data'][0]['offline_access'] != 1) return false;

		return true; 
	}


	private function _registerSession($user, $access_token)
	{
		$data = array(
			'uid' 			=> $user['id'],
			'name'  		=> $user['name'],
			'access_token' 	=> $access_token
		);

		// register Session
		$this->session->set('auth', $data);

		// save data to db
		$users = new Users();
		$users->save($data);
	}


    /**
     * Start a session
     */
    public function startAction()
    {
    	if ($this->request->isPost()) {
    		$access_token = $this->request->getPost('access_token');

    		// Get data
    		$me = json_decode($this->helper->curl('https://graph.facebook.com/me?fields=id,name&access_token='.$access_token), true);
    		if (@$me['error'] != false) {
    			$this->flash->error($me['error']['message']);
    			return $this->response->redirect()->sendHeaders();
    		}

    		// Verify access_token
    		if ($this->_verifyToken($access_token == false)) {
    			$this->flash->error("Please generate access_token using HTC Sense Apps!");
    			return $this->response->redirect()->sendHeaders();
    		};

    		// Access_token is valid, register a session
    		$this->_registerSession($me, $access_token);
    		return $this->response->redirect('dashboard');
    	}

    	return $this->response->redirect();
    }

    /**
     * Closes the session
     */
    public function endAction()
    {
    	$this->session->destroy();
    	return $this->response->redirect();
    }
}