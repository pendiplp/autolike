<?php

/**
 * LikerController
 *
 * Controller untuk Pilihan Service Autoliker ( status, photo, custom, fanspagepost )
 */
class LikerController extends ControllerBase
{
	private $access_token;

	public function initialize()
	{
		$auth = $this->session->get('auth');
		if ($auth == false) {
			return $this->response->redirect();
		}

		$this->access_token = $auth['access_token'];
	}

    /**
     * Process Liker 
     * Router: /liker/process
     *
     * @param int $_POST['fbid']
     * @param int $_POST['limit']
     */
	public function processAction()
	{
		$this->view->disable();
		if ($this->request->isPost()) {
			//waktu sekarang
			$now  = date("Y-m-d H:i:s");

			$uid  = $this->session->get('auth')['uid'];
			$user = Users::findFirstByUid($uid);

			if ($user->last_submit != false) {
				// Tambahkan 15minutes ke last_submit
				$last_submit = date_create($user->last_submit)->modify('+15 Minutes')->format('Y-m-d H:s:i');

				/**
				 * Bandingkan waktu sekarang dengan last_submit+15minutes
				 * Jika waktu sekarang < last_submit+15minutes kembalikan ke dashboard
				 */
				if ($now < $last_submit) {
					return $this->response->redirect('dashboard');	
				}
			}

			// Jalankan proses liker
			$fbid 	= $this->request->getPost('fbid');
			$limit 	= $this->request->getPost('limit');
			if ($limit > 350) $limit = 350;

			// Ambil token dari tabel 'users' secara random sebanyak $limit
			$tokens = Users::find(
				array(
					'order' => "rand()",
					'limit' => $limit
					)
				);

			// Send like ke status ( $fbid )
			foreach ($tokens as $value) {
				$this->helper->curl('https://graph.facebook.com/' . $fbid . '/likes?method=post&access_token=' . $value->access_token);
			}

			// Update last_submit
			$user->last_submit = date("Y-m-d H:i:s");
			$user->save();
			return $this->response->redirect('dashboard');
		}
		return $this->response->redirect();
	}

	public function indexAction()
	{
		$this->response->redirect('dashboard');
	}

	/**
	 * Pilih Status
	 * Router: /liker/status
	 */
	public function statusAction()
	{
		$feed = new Feeds();
		$this->view->feeds = $feed->getStatus();
	}

	/**
	 * Pilih Photo Album
	 * Router: /liker/photo
	 */
	public function photoAction()
	{
		$feed = new Feeds();
		$this->view->albums = $feed->getPhotoAlbum();
	}

	/**
	 * Pilih Photo
	 * Router: /liker/photo/album/id/{album_id:[0-9]+}
	 */
	public function photoAlbumAction()
	{
		$album_id = $this->dispatcher->getParam("album_id");
		
		$feed = new Feeds();
		$this->view->photos = $feed->getPhotos($album_id);
	}

	/**
	 * Pilih video
	 * Router: /liker/video
	 */
	public function videoAction()
	{
		$feed = new Feeds();
		$this->view->videos = $feed->getVideos();
	}

	/**
	 * Pilih Catatan
	 * Router: /liker/note
	 */
	public function noteAction()
	{
		$feed = new Feeds();
		$this->view->notes = $feed->getNotes();
	}

	/**
	 * Pilih fanspage ID
	 * Router: /liker/page
	 */
	public function pageAction()
	{
		$feed = new Feeds();
		$this->view->pages = $feed->getPageID();
	}

	/**
	 * Pilih Status dari fanspage
	 * Router: /liker/page/id/{page_id:[0-9]+}
	 */
	public function pageStatusAction()
	{
		$page_id = $this->dispatcher->getParam("page_id");
		
		$feed = new Feeds();
		$this->view->feeds = $feed->getStatus($page_id);
	}

	/**
	 * Custom Liker
	 * Router: /liker/custom
	 */
	public function customAction()
	{

	}

}