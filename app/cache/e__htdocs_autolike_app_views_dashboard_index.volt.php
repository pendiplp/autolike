    <!-- Content -->
    <div class="container text-center">
    	<div class="jumbotron">
    		<img src="<?= $this->url->get('img/casper.png') ?>" class="img-rounded">
    		<h1>Welcome to Dashboard</h1>
    		<p class="lead">Select One Of Service As You Want</p>
    	</div>
    </div>

    <div class="container text-center">
    	<div class="panel panel-info">
    		<div class="panel-heading">
    			<h3 class="panel-title">Dashboard</h3>
    		</div>
    		<div class="panel-body">
    			<div class="input-btn">
                    <?= $this->tag->linkTo(['liker/status', 'STATUS LIKER', 'class' => 'btn btn-primary']) ?>
                    <?= $this->tag->linkTo(['liker/photo', 'PHOTO LIKER', 'class' => 'btn btn-primary']) ?>
                    <?= $this->tag->linkTo(['liker/video', 'VIDEO LIKER', 'class' => 'btn btn-primary']) ?>
                </div>
                <br/>
                <div class="input-btn">
                    <?= $this->tag->linkTo(['liker/note', 'NOTE LIKER', 'class' => 'btn btn-primary']) ?>
                    <?= $this->tag->linkTo(['liker/page
                    ', 'FANSPAGE POST LIKER', 'class' => 'btn btn-primary']) ?>
                    <?= $this->tag->linkTo(['liker/custom', 'CUSTOM LIKER', 'class' => 'btn btn-primary']) ?>
    			</div>
    			<br/>
    			<div class="input-btn">
                    <?= $this->tag->linkTo(['session/end', 'LOGOUT', 'class' => 'btn btn-warning']) ?>
    				<!-- <button type="Button" class="btn btn-warning">LOGOUT</button> -->
    			</div>


    		</div>
    	</div>
    </div>