<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">STATUS LIKER</h3>
		</div>
	</div>
</div>
<?php foreach ($feeds as $status) { ?>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<img src="http://graph.facebook.com/<?= $status['from']['id'] ?>/picture?type=large&redirect=true&width=120&height=120" alt="fbid" class="img-circle pull-left">
			<h3><?= $status['from']['name'] ?></h3>
			<div>
				<?= $status['message'] ?>
			</div><br/>
            <?= $this->tag->form(['liker/process']) ?>
	            <select class="col-md-12" name="limit">
	            	<option value="250">Set Your Limit</option>
	            	<option value="50">50</option>
	            	<option value="100">100</option>
	            	<option value="150">150</option>
	            	<option value="200">200</option>
	            	<option value="250">250</option>
	            	<option value="300">300</option>
	            </select>
	            <input type="hidden" name="fbid" value="<?= $status['fbid'] ?>" />
				<?= $this->tag->submitButton(['Submit', 'class' => 'btn btn-primary']) ?>
			<?= $this->tag->endForm() ?>
		</div>
	</div>
</div>
<?php } ?>