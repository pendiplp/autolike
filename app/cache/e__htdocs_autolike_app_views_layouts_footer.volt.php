    <!-- footer -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h5 class="white-text">About us</h5>
            <p class="grey-text text-lighten-4">We are a team of college students from Indonesia working on this project like it's our full time job. Any amount of donation would help support and continue development on this project and is greatly appreciated.</p>
          </div>
          <div class="col-md-3">
            <h5 class="white-text">Other Services</h5>
            <ul>
              <li><a target="_blank" class="white-text" href="//member.Casper.me" rel="dofollow">VIP Membership</a></li>
              <li><a target="_blank" class="white-text" href="//reaction.Casper.me" rel="dofollow">Casper Reaction</a></li>
              <li><a target="_blank" class="white-text" href="//follow.Casper.me" rel="dofollow">Casper Followers</a></li>
              <li><a target="_blank" class="white-text" href="//fanspage.Casper.me" rel="dofollow">Casper Fanspage</a></li>
              <li><a target="_blank" class="white-text" href="//Caspergram.me" rel="dofollow">Casper Instagram</a></li>
            </ul>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </nav>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
    <?= $this->tag->javascriptInclude('js/jquery-3.1.0.min.js') ?>

    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
    <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>