<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Indonesia Auto Like</title>

    <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <?= $this->tag->stylesheetLink('css/bootstrap.min.css') ?>

    <!-- Optional theme -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
    <?= $this->tag->stylesheetLink('css/bootstrap-theme.min.css') ?>

    <!-- Custom styles for this template -->
    <?= $this->tag->stylesheetLink('css/custom.css') ?>
  </head>
  <body>
        <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Casper Liker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav pull-right">
            <li class="active"><a href="<?= $this->url->get('') ?>">Home</a></li>
            <li><a href="<?= $this->url->get('terms') ?>">Terms</a></li>
            <li><a href="<?= $this->url->get('privacy') ?>">Privacy</a></li>
            <li><a href="<?= $this->url->get('contact') ?>">Contact</a></li>
            <li><a href="<?= $this->url->get('about') ?>">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    
    <?= $this->getContent() ?>

        <!-- footer -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h5 class="white-text">About us</h5>
            <p class="grey-text text-lighten-4">We are a team of college students from Indonesia working on this project like it's our full time job. Any amount of donation would help support and continue development on this project and is greatly appreciated.</p>
          </div>
          <div class="col-md-3">
            <h5 class="white-text">Other Services</h5>
            <ul>
              <li><a target="_blank" class="white-text" href="//member.Casper.me" rel="dofollow">VIP Membership</a></li>
              <li><a target="_blank" class="white-text" href="//reaction.Casper.me" rel="dofollow">Casper Reaction</a></li>
              <li><a target="_blank" class="white-text" href="//follow.Casper.me" rel="dofollow">Casper Followers</a></li>
              <li><a target="_blank" class="white-text" href="//fanspage.Casper.me" rel="dofollow">Casper Fanspage</a></li>
              <li><a target="_blank" class="white-text" href="//Caspergram.me" rel="dofollow">Casper Instagram</a></li>
            </ul>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </nav>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
    <?= $this->tag->javascriptInclude('js/jquery-3.1.0.min.js') ?>

    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
    <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
  </body>
</html>