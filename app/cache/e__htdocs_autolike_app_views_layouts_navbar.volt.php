    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Casper Liker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav pull-right">
            <li class="active"><a href="<?= $this->url->get('') ?>">Home</a></li>
            <li><a href="<?= $this->url->get('terms') ?>">Terms</a></li>
            <li><a href="<?= $this->url->get('privacy') ?>">Privacy</a></li>
            <li><a href="<?= $this->url->get('contact') ?>">Contact</a></li>
            <li><a href="<?= $this->url->get('about') ?>">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>