<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">CUSTOM LIKER</h3>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body ">
			<?= $this->tag->form(['liker/process']) ?>
				<div class="input-group col-md-12">
					<input type="text" name="fbid" class="form-control" placeholder="Your ID here...">
				</div>
				<select class="col-md-12" name="limit">
					<option value="250">Set Your Limit</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="150">150</option>
					<option value="200">200</option>
					<option value="250">250</option>
					<option value="300">300</option>
				</select>
				<?= $this->tag->submitButton(['Submit', 'class' => 'btn btn-primary']) ?>
			<?= $this->tag->endForm() ?>
		</div>
	</div>
</div>